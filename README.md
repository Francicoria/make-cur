# make-cur

cli program to facilitate cursor file (.cur) creation on Windows.

To compile and run:
```console
$ ./build.sh   # or .\build.cmd on Windows
$ ./make-cur <input>.png <input width> <input height> <hotspot x> <hotspot y> <palette colors count>
```

Resources on ICO/CUR file format:
- <https://en.wikipedia.org/wiki/ICO_(file_format)#Icon_file_structure>
- <https://devblogs.microsoft.com/oldnewthing/20101018-00/?p=12513>
