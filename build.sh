#!/usr/bin/env sh

gcc -Wall -Wextra -Og -o make-cur make-cur.c common.c
gcc -Wall -Wextra -Og -o make-ani make-ani.c common.c
