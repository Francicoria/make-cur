#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "common.h"

void usage(char *prog_name) {
	fprintf(stderr, "USAGE:\n");
	fprintf(stderr, "  %s <input>.png <width> <height> <hotspot x> <hotspot y> <palette colors count>\n", prog_name);
}

int main(int argc, char *argv[]) {
	// ./build-cur <input>.png <width> <height> <hotspot x> <hotspot y> <palette colors count>
	if (argc == 1) {
		fprintf(stderr, "Not enough arguments provided\n");
		usage(argv[0]);
		return 1;
	}
	char *prog_name = get_arg(&argc, &argv, NULL);


	/// cli arguments handling
	char *img_filepath = get_arg(&argc, &argv, prog_name);

	size_t width  = str_to_num(get_arg(&argc, &argv, prog_name));
	size_t height = str_to_num(get_arg(&argc, &argv, prog_name));
	printf("Image size: %zux%zupx\n", width, height);

	size_t hotspot_x = str_to_num(get_arg(&argc, &argv, prog_name));
	size_t hotspot_y = str_to_num(get_arg(&argc, &argv, prog_name));
	printf("Hotspot: (x=%zupx, y=%zupx)\n", hotspot_x, hotspot_y);

	size_t palette_count = str_to_num(get_arg(&argc, &argv, prog_name));
	if (palette_count == 0) {
		fprintf(stderr, "Invalid number of colors in the palette provided (%zu).", palette_count);
		return 1;
	}
	printf("%zu color%c in palette.\n", palette_count, palette_count == 1 ? ' ': 's');
	/// cli arguments handling

	FILE *img_file = fopen(img_filepath, "rb");
	if (img_file == NULL) {
		fprintf(stderr, "Couldn't open file %s for reading\n", img_filepath);
		return 1;
	}

	string out_filepath = copy_cstring(img_filepath);
	remove_file_extension(&out_filepath);

	append_file_extension(&out_filepath, "cur");

	FILE *out_file = fopen(out_filepath.content, "wb");
	if (out_file == NULL) {
		fprintf(stderr, "Couldn't open file %s for writing\n", out_filepath.content);
		goto free_and_return;
	}

	long img_size = get_file_size(img_file);

	{
		ICONDIR header = {
			.type = 2,

			.images_count = 1,
			.images = {
				(ICONDIRENTRY){
					.width  = (uint8_t)width,
					.height = (uint8_t)height,
					.colors_count = (uint8_t)palette_count,
					.hotspot_x = (uint16_t)hotspot_x,
					.hotspot_y = (uint16_t)hotspot_y,
					.image_size = img_size,
				},
			},
		};
		header.images[0].image_offset = sizeof(header);

		// Write the header
		if (fwrite((void *)&header, sizeof(header), 1, out_file) < 1) {
			fprintf(stderr, "Couldn't write to file %s\n", out_filepath.content);
			goto free_and_return;
		}
	}

	{
		char *buffer = malloc(img_size);
		assert(buffer != NULL);

		fread(buffer, 1, img_size, img_file);

		// Write the image data
		if (fwrite((void *)buffer, 1, img_size, out_file) < 1) {
			fprintf(stderr, "Couldn't write to file %s\n", out_filepath.content);
			free(buffer);
			goto free_and_return;
		}
		free(buffer);
	}

	printf("Successfully wrote to file %s\n", out_filepath.content);

	fclose(img_file);
	fclose(out_file);
	free_string(&out_filepath);
	return 0;

free_and_return:
	if (out_file) fclose(out_file);
	if (img_file) fclose(img_file);
	if (out_filepath.content) free_string(&out_filepath);
	return 1;
}
