#ifndef COMMON_H_
#define COMMON_H_

#include <stdint.h>

// ICO/CUR format resources:
// https://en.wikipedia.org/wiki/ICO_(file_format)#Icon_file_structure
// https://devblogs.microsoft.com/oldnewthing/20101018-00/?p=12513
typedef struct {
	uint8_t width;
	uint8_t height;
	uint8_t colors_count;
	uint8_t _res;

	// ICO: color planes, either 0 or 1
	// CUR: x coords for hotspot
	uint16_t hotspot_x;

	// ICO: bits per pixel
	// CUR: y coords for hotspot
	uint16_t hotspot_y;

	uint32_t image_size; // in bytes
	
	uint32_t image_offset; // from the beggining of the file (so it's equal to the header size in bytes)
} __attribute__((packed)) ICONDIRENTRY;

typedef struct {
	uint16_t _res;
	uint16_t type;

	uint16_t images_count;
	ICONDIRENTRY images[1];
} __attribute__((packed)) ICONDIR;

#define AF_ICON     0x0001L
#define AF_SEQUENCE 0x0002L

typedef struct {
	uint32_t size; // Num bytes in AniHeader (36 bytes)
	uint32_t frames_count; // Number of unique Icons in this cursor
	uint32_t cSteps; // Number of Blits before the animation cycles @WTF does that mean?? test with random values like 0, 10, 60, ...
	uint32_t _res0, _res1; // reserved, must be zero.
	uint32_t _res2, _res3; // reserved, must be zero.
	uint32_t jif_rate; // Default Jiffies (1/60th of a second) if rate chunk not present.
	uint32_t flags; // Animation Flag (see AF_ constants)
} __attribute__((packed)) ANIHeader;

void usage(char *prog_name);

long get_file_size(FILE *file);

char *get_arg(int *argc, char ***argv, char *prog_name);

size_t str_to_num(char *str);

typedef struct {
	char *content;
	size_t size;
	size_t cap;
} string;
string copy_cstring(char *src);
void free_string(string *str);
void remove_file_extension(string *str);
void append_file_extension(string *str, char *ext);

#endif // COMMON_H_
