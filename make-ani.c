#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "common.h"

uint32_t file_size = 0; // in bytes

void usage(char *prog_name) {
	fprintf(stderr, "USAGE:\n");
	fprintf(stderr, "  %s\n", prog_name);
}

void ascii_write(FILE *f, char txt[4]) {
	size_t result = fwrite(txt, 1, 4, f);
	if (result < 4) {
		fprintf(stderr, "Couldn't write chunk's ascii identifier correctly\n");
		fclose(f);
		return;
	}

	file_size += 4;
}

void u32_write_placeholder(FILE *f) {
	uint32_t n = 0;
	size_t result = fwrite((void *)&n, 4, 1, f);
	if (result < 1) {
		fprintf(stderr, "Couldn't write placeholder number (u32) correctly\n");
		if (f) fclose(f);
		return;
	}
}

void u32_write(FILE *f, uint32_t chunk_lenght) { // chunk lenght doesn't include the ascii identifier and the u32

	union {
		uint32_t _u32;
		char bytes[4];
	} data = {chunk_lenght};

	size_t result = fwrite(data.bytes, 1, 4, f);
	if (result < 4) {
		fprintf(stderr, "Couldn't write chunk's lenght (u32) correctly\n");
		if (f) fclose(f);
		return;
	}

	file_size += 4;
}

void data_write(FILE *f, uint8_t *data, size_t lenght) {
	size_t result = fwrite(data, 1, lenght, f);
	if (result < lenght) {
		fprintf(stderr, "Couldn't write chunk's data correctly\n");
		if (f) fclose(f);
		return;
	}
	if (lenght % 2 != 0) fputc(0, f); // pad a byte when the lenght of the chunck is not even

	file_size += lenght + (lenght % 2 != 0);
}

int main(void) {
	const char *out_filepath = "out.ani";

	FILE *f = fopen(out_filepath, "wb");
	if (f == NULL) {
		fprintf(stderr, "Couldn't open file %s for writing\n", out_filepath);
		goto free_and_return;
	}

	ascii_write(f, "RIFF");
	long file_lenght_file_position = ftell(f);
	u32_write_placeholder(f); // file lenght
	ascii_write(f, "ACON");

	/* (Optional) file information
	ascii_write(f, "LIST");
	u32_writef, 0);
	ascii_write(f, "INFO");

	//Title
	ascii_write(f, "INAM");
	u32_write(f, 0);

	// Artist/Author
	ascii_write(f, "IART");
	u32_write(f, 0);
	*/

	ascii_write(f, "anih");
	u32_write(f, sizeof(ANIHeader));
	union {
		ANIHeader _head;
		uint8_t bytes[sizeof(ANIHeader)];
	} data = { (ANIHeader){
		.size = sizeof(ANIHeader),
		.frames_count = 1,
		.cSteps = 1,
		.jif_rate = 1,
		.flags = AF_ICON,
	} };
	data_write(f, data.bytes, sizeof(ANIHeader));

	ascii_write(f, "rate");
	u32_write(f, 4);
	u32_write(f, 1);

	ascii_write(f, "seq ");
	u32_write(f, 4);
	u32_write(f, 1);

	ascii_write(f, "LIST");
	long frames_size_file_position = ftell(f);
	u32_write_placeholder(f); // size of the frames array in bytes
	ascii_write(f, "fram");
	/// This should be in a loop, from the 0..frames_count-1 (in ANIHeader)
	{
		ascii_write(f, "icon");
		const char *img_filepath = "test.png";
		FILE *img_file = fopen(img_filepath, "rb");
		if (img_file == NULL) {
			fprintf(stderr, "Couldn't open file %s for reading\n", img_filepath);
			goto free_and_return;
		}
		long img_size = get_file_size(img_file);
		size_t width = 32;
		size_t height = 32;
		size_t hotspot_x = 6;
		size_t hotspot_y = 6;

		ICONDIR header = {
			.type = 2,

			.images_count = 1,
			.images = {
				(ICONDIRENTRY){
					.width  = (uint8_t)width,
					.height = (uint8_t)height,
					.colors_count = 2,
					.hotspot_x = (uint16_t)hotspot_x,
					.hotspot_y = (uint16_t)hotspot_y,
					.image_size = img_size,
				},
			},
		};
		header.images[0].image_offset = sizeof(header);

		// Write the header
		union {
			ICONDIR _icondir;
			uint8_t bytes[sizeof(ICONDIR)];
		} data = { header };
		//data_write(f, data.bytes, sizeof(header));

		size_t buffer_size = sizeof(data.bytes) + img_size;
		uint8_t *buffer = malloc(buffer_size);
		if (buffer == NULL) {
			fclose(img_file);
			goto free_and_return;
		}

		memcpy(buffer, data.bytes, sizeof(data.bytes));
		fread(buffer + sizeof(data.bytes), 1, img_size, img_file);

		// Write the image data
		u32_write(f, buffer_size);
		data_write(f, (uint8_t *)buffer, buffer_size);
		free(buffer);
		{ // Go back to set the size of the frames array
			long here = ftell(f);
			fseek(f, frames_size_file_position, SEEK_SET);
			// account for: "icon", buffer_size, buffer
			u32_write(f, 4 + 4 + buffer_size);
			fseek(f, here, SEEK_SET);
		}
	}
	///

	// Go back to set the size of the file
	fseek(f, file_lenght_file_position, SEEK_SET);
	// account for: "RIFF", file_lenght
	u32_write(f, file_size - 4 - 4);

	fclose(f);
	return 0;

free_and_return:
	if (f) fclose(f);
	return 1;
}
