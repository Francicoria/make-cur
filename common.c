#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

long get_file_size(FILE *file) {
	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	fseek(file, 0, SEEK_SET);

	return size;
}

char *get_arg(int *argc, char ***argv, char *prog_name) {
	if (*argc == 0) {
		fprintf(stderr, "Not enough arguments provided\n");
		usage(prog_name);
		exit(1);
	}

	*argc -= 1;
	*argv += 1;
	return *(*argv - 1);
}

size_t str_to_num(char *str) {
	size_t n = 0;
	while (*str != '\0') {
		int m = *str - '0';
		str++;
		if (m > 9 || m < 0) continue; // if it encounters other chars in number, just skip 'em
		n = (10 * n) + m;
	}
	return n;
}

string copy_cstring(char *src) {
	size_t i = 0;
	while (src[i] != '\0') i += 1;
	if (i == 0) return (string){0};
	char *str = calloc(i + 1, 1);
	assert(str != NULL);
	memcpy(str, src, i + 1);
	return (string){ .content = str, .size = i, .cap = i + 1 };
}

void free_string(string *str) {
	if (str->content == NULL) return;
	free(str->content);
	str->size = str->cap = 0;
}

void remove_file_extension(string *str) {
	for (size_t i = 0; str->content[i] != '\0'; i += 1) {
		if (str->content[i] == '.') {
			str->content[i] = '\0';
			str->size = i;
			return;
		}
	}
	fprintf(stderr, "\"%s\" has no file extension\n", str->content);
	exit(1);
}

void append_file_extension(string *str, char *ext) {
	assert(ext != NULL);
	size_t i = 0;
	while(ext[i] != '\0') i += 1;

	if (str->size + 1 + i + 1 > str->cap) {
		str->cap = str->size + 1 + i + 1;
		void *result = realloc(str->content, str->cap);
		assert(result != NULL);
	}
	void *result = memcpy(str->content + str->size + 1, ext, i);
	assert(result != NULL);
	*(str->content + str->size) = '.';
    //  filename     .  ext  \0
	str->size += 1 + i + 1;
}
